#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <conio.h>
using namespace std;

struct nodo{
	int valor;
	struct nodo *sgte;
	struct nodo *ant;
};

typedef struct nodo *Tlista;

void mergeSort(Tlista &lista);
void partir(Tlista &cab, Tlista &prim, Tlista &fin);
Tlista listando(Tlista &a, Tlista &b);
int obtenerCifrasTotales(int dato);//para la maxima cantidad de cifras de un nro
Tlista eliminarInicioRadix(Tlista &cab);//Es casi igual al del labo de pilas y Colas
int obtenerCifraEspecifica(int dato, int pos);//obtiene la cifra de una pos determinada, o devuelve 0
void insertarFinal(Tlista &lista, Tlista nuevo);//Inserto un Nodo especifico
void mostrar(Tlista &f);
void RadixSort(Tlista &cab);
void Quick_Sort(Tlista &cab);
void Sort_shell(Tlista &cab);
void insertarFinal(Tlista &lista, int dato);
void insercion(Tlista &lista);
void burbuja(Tlista &lista);
void seleccion(Tlista &lista);
void intercambio(Tlista &f);
void mostrar (Tlista &lista);
void programa(Tlista &lista);
string menu(Tlista &lista);
int convertir(string num, bool &bandera);

int main(){

	Tlista lista = NULL;
	Tlista lista_2=NULL;
	int op,numero;
	programa(lista);
}

void programa(Tlista &f)
{
    string op;
    int x;
    do
    {
        op = menu(f);

        if(op.size() > 1 || op.size() == 0)
            cout << "\tHas escrito demasiados caracteres!"<<endl;
        else
        {
            if(op.compare("1") == 0)
            {
                cout<<"\n\tHas elegido la opcion 1"<<endl;
                cout<<"\n\tIngrese un numero: ";
                cin>>x;
                insertarFinal(f,x);

            }
            else if(op.compare("2") == 0)
            {
                cout<<"\n\tHas elegido la opcion 2(RADIX)"<<endl;
                RadixSort(f);
                mostrar(f);
            }
            else if(op.compare("3") == 0)
            {
				cout<<"\n\tHas elegido la opcion 3(BURBUJA)"<<endl;
				cout<<"\n\tBurbuja"<<endl;
				burbuja(f);
            }
            else if(op.compare("4") == 0)
            {
				cout<<"\n\tHas elegido la opcion 4(INSERCION)"<<endl;
				cout<<"\n\tInsercion"<<endl;
				insercion(f);
            }
            else if(op.compare("5") == 0)
            {
				cout<<"\n\tHas elegido la opcion 5(INTERCAMBIO)"<<endl;
				cout<<"\n\tIntercambio"<<endl;
				intercambio(f);
            }
            else if(op.compare("6") == 0)
            {
				cout<<"\n\tHas elegido la opcion 6(SELECCION)"<<endl;
				cout<<"\n\tSeleccion"<<endl;
				seleccion(f);
            }
            else if(op.compare("7") == 0)
            {
				cout<<"\n\tHas elegido la opcion 7(SHELL SORT)"<<endl;
				Sort_shell(f);
            }
            else if(op.compare("8") == 0)
            {
				cout<<"\n\tHas elegido la opcion 8(QUICK SORT)"<<endl;
				Quick_Sort(f);
            }
            else if(op.compare("9") == 0)
            {
				cout<<"\n\tHas elegido la opcion 9(MERGE)"<<endl;
				mergeSort(f);
				mostrar(f);
            }
            else if(op.compare("0") == 0)
            {
				cout<<"\n\tHas elegido la opcion 0(SALIR)"<<endl;
				system("pause");
            }

        }

        cin.ignore(256,'\n');
    }while(op != "0" && op!= "2" && op != "9" );
}

string menu(Tlista &f)
{
    system("cls");
    string op;
    	cout<<endl;

        cout<<"\t\t\t\t\t\t"<<endl;
        cout<<"\t\t\tMENU\t\t\t"<<endl;
        cout<<"\t\t\t\t\t\t"<<endl;
        cout<<"\t1. Agregar elemento a la lista\t\t"<<endl;
        cout<<"\t2. Ordenar por Radix y salir\t\t"<<endl;
        cout<<"\t3. Ordenar por Burbuja\t\t"<<endl;
        cout<<"\t4. Insertar por Insercion\t\t"<<endl;
        cout<<"\t5. Ordenar por Intercambio\t\t"<<endl;
        cout<<"\t6. Ordenar por Seleccion\t\t"<<endl;
        cout<<"\t7. Ordenar por Shell Sort\t\t"<<endl;
        cout<<"\t8. Ordenar por Quick Sort\t\t"<<endl;
        cout<<"\t9. Ordenar por Merge y salir\t\t"<<endl;
        cout<<"\t0. Salir\t\t\t\t"<<endl;
        cout<<"\t\t\t\t\t\t"<<endl;


        mostrar(f);
        cout<<endl;

        cout<<"\n\tSeleccionar opcion: ";
        cin>>op;


        cin.ignore(256,'\n');
    return op;
}

int obtenerCifrasTotales(int dato)
{
	int temp = dato;
	int c = 0;
	while(temp!=0)
	{
		temp = temp/10;
		c++;
	}

	return c;

}

int obtenerCifraEspecifica(int dato, int pos)
{
	int temp = dato;
	int cifra;
	int c = 0;
	while(temp!=0 && c!=pos)
	{
		cifra = temp%10;
		temp = temp/10;
		c++;
	}

	if(c != pos)
		return 0;

	return cifra;
}

Tlista eliminarInicioRadix(Tlista &cab)
{
	if( cab == NULL)
		return NULL;
	else
	{
		Tlista temp = cab;

		cab = cab->sgte;
		if(cab != NULL)
			cab->ant = NULL;

		temp->sgte = NULL;

		return temp;

	}
}

void insertarFinal(Tlista &lista, Tlista nuevo){

	Tlista p = lista;

	if(lista == NULL){
		lista = nuevo;
	}else{
		if(lista->sgte == NULL){
			lista->sgte = nuevo;
		}else{
			while(p->sgte != NULL){
				p = p->sgte;
			}
			p->sgte = nuevo;
		}
	}
}
void partir(Tlista &cab, Tlista &prim, Tlista &fin){

	Tlista aux1 = NULL;
	Tlista aux2 = NULL;

	if (cab == NULL || cab->sgte == NULL){
		prim = cab;
		fin = NULL;
	}
	else{
		aux2 = cab;
		aux1 = cab->sgte;

		while (aux1 != NULL){
			aux1 = aux1->sgte;

			if (aux1 != NULL){
				aux2 = aux2->sgte;
				aux1 = aux1->sgte;
			}
		}

		prim = cab;
		fin = aux2->sgte;
		aux2->sgte = NULL;
	}
}

Tlista listando(Tlista &a, Tlista &b){

	Tlista p = NULL;

	if (a == NULL){
		return b;
	}
	else if (b == NULL){
		return a;
	}

	if (a->valor <= b->valor){
		p = a;
		p->sgte = listando(a->sgte, b);
	}
	else{
		p = b;
		p->sgte = listando(a, b->sgte);
	}

	return p;
}

void mergeSort(Tlista &lista){

	Tlista p = lista;
	Tlista a = NULL;
	Tlista b = NULL;

	if (p == NULL || p->sgte == NULL){

		return;
	}

	partir(p, a, b);

	mergeSort(a);
	mergeSort(b);

	lista = listando(a, b);
}


void RadixSort(Tlista &cab)
{
	if( cab != NULL )
	{
		int max = 0;
		int total = 0;

		Tlista temp = cab;

		while(temp != NULL)
		{
			total++;

			if(temp->valor > max)
				max = temp->valor;
			temp = temp->sgte;
		}

		int cifras_maximas = obtenerCifrasTotales(max);
		int cifra;

		//Como el maximo tiene "cifras_maximas" cifras, el algoritmo se hara esa cantidad de veces
		for(int i = 0;  i < cifras_maximas ; i++)
		{
			//Necesito 10 colas, para ordenar
			Tlista cola_0 = NULL;
			Tlista cola_1 = NULL;
			Tlista cola_2 = NULL;
			Tlista cola_3 = NULL;
			Tlista cola_4 = NULL;
			Tlista cola_5 = NULL;
			Tlista cola_6 = NULL;
			Tlista cola_7 = NULL;
			Tlista cola_8 = NULL;
			Tlista cola_9 = NULL;

			while(cab != NULL)
			{
				//Obtengo el primer Elemento de la Lista y lo saco
				temp = eliminarInicioRadix(cab);
				cifra = obtenerCifraEspecifica(temp->valor,i+1);//Obtengu su cifra de acuerdo a la pos
				//Se introduce en la cola respectiva
				switch(cifra)
				{
					case 0:
						insertarFinal(cola_0,temp);
						break;

					case 1:
						insertarFinal(cola_1,temp);
						break;

					case 2:
						insertarFinal(cola_2,temp);
						break;

					case 3:
						insertarFinal(cola_3,temp);
						break;

					case 4:
						insertarFinal(cola_4,temp);
						break;

					case 5:
						insertarFinal(cola_5,temp);
						break;

					case 6:
						insertarFinal(cola_6,temp);
						break;

					case 7:
						insertarFinal(cola_7,temp);
						break;

					case 8:
						insertarFinal(cola_8,temp);
						break;

					case 9:
						insertarFinal(cola_9,temp);
						break;

				}

			}

			//Reenlazo la lista
			insertarFinal(cab,cola_0);
			insertarFinal(cab,cola_1);
			insertarFinal(cab,cola_2);
			insertarFinal(cab,cola_3);
			insertarFinal(cab,cola_4);
			insertarFinal(cab,cola_5);
			insertarFinal(cab,cola_6);
			insertarFinal(cab,cola_7);
			insertarFinal(cab,cola_8);
			insertarFinal(cab,cola_9);
		}



	}
}




 void insertarFinal(Tlista &lista, int dato){

	Tlista p = lista;
	Tlista nuevo = new (struct nodo);
	nuevo->sgte = NULL;
	nuevo->ant = NULL;
	nuevo->valor = dato;

	if(lista == NULL){
		lista = nuevo;
	}else{
		if(lista->sgte == NULL){
            nuevo->ant = lista;
			lista->sgte = nuevo;
		}else{
			while(p->sgte != NULL){
				p = p->sgte;
			}
			nuevo->ant = p;
			p->sgte = nuevo;

		}
	}
}
void intercambio(Tlista &f){

	Tlista p,q;

	int n=1,i=1, j,tem;
	bool cam;

	if(f==NULL)
		cout<<"\tNo hay nada que ordenar"<<endl;

	else{
		p=f;
		while(p->sgte!=NULL){
			p=p->sgte;
			n++;
		}
		p=f;

		do{
			q=p;
			j=i+1;
			while(j<=n){
				q=q->sgte;

				if(p->valor>q->valor){
					tem=q->valor;
					q->valor=p->valor;
					p->valor=tem;
				}
				j++;
			}
			p=p->sgte;
			i++;
	}while(i<=n);

	}


}

void insercion(Tlista &lista){
	int numero;
	bool f;
	string numero_aux;
	cout<<"\tIngrese un numero: "; cin>>numero_aux;

	numero = convertir(numero_aux,f);

	if(f == 0)
	{
		cout << "\tIngreso un numero Invalido";
		getch();
		return;
	}

	Tlista aux = NULL;
	Tlista nuevo = new (struct nodo);
	nuevo->valor = numero;
	nuevo->sgte = NULL;
	nuevo->ant = NULL;
	Tlista p = lista;
	Tlista final = lista;
	if(lista == NULL){
		lista = nuevo;
	}else{
		if(lista->sgte == NULL){
			if(nuevo->valor <= lista->valor){
				nuevo->sgte = lista;
				lista->ant = nuevo;
				lista = nuevo;
			}else{
			    nuevo->ant = lista;
				lista->sgte = nuevo;
			}
		}else{
			while(final->sgte != NULL){
				final = final->sgte;
			}

			if(numero<=lista->valor){
				nuevo->sgte=lista;
				lista->ant=nuevo;
				lista=nuevo;
			}else if(numero>=final->valor){
			    nuevo->ant = final;
				final->sgte=nuevo;
			}else{
				while(p->valor<=numero){
					aux=p;
					p=p->sgte;
				}
				nuevo->sgte=p;
				nuevo->ant=aux;
				aux->sgte=nuevo;
				p->ant=nuevo;
			}
		}
	}
}

void Quick_Sort(Tlista &cab)
{
	if (cab != NULL)
	{
		int contador = 0;
		int n = 2;
		int contador1 = 0;
		Tlista aux = cab->sgte;
		Tlista sombrero = NULL;
		Tlista pivote;

		Tlista cab_aux = cab;
		Tlista final_aux = NULL;
		Tlista cab_s2 = cab;

		int paso = 0;


		bool orientacion = true;

		while (aux->sgte != NULL)
		{
			aux = aux->sgte;
			n++;
		}

		if (n == 1)
			return;
		else
		{

			pivote = cab;
			sombrero = aux;

			while (true)
			{
				if (pivote == NULL || sombrero == NULL)
					return;


				orientacion = true;
				paso = 0;
				while (pivote != sombrero)
				{
					if (orientacion)
					{

						if (pivote->valor > sombrero->valor)
						{
							int temp = sombrero->valor;
							sombrero->valor = pivote->valor;
							pivote->valor = temp;

							Tlista temp1 = sombrero;
							sombrero = pivote;
							pivote = temp1;
							orientacion = false;
						}
						else
						{
							sombrero = sombrero->ant;
							paso++;
						}

					}
					else
					{
						if (pivote->valor < sombrero->valor)
						{
							int temp = sombrero->valor;
							sombrero->valor = pivote->valor;
							pivote->valor = temp;

							Tlista temp1 = sombrero;
							sombrero = pivote;
							pivote = temp1;

							orientacion = true;
						}
						else
							sombrero = sombrero->sgte;
					}
				}
				contador1++;
				contador++;
				if (contador1 == 1)
					cab_aux = pivote;
				sombrero = pivote->ant;
				pivote = cab_s2;


				if (sombrero == final_aux)
				{
					if (cab_aux != cab_s2)
						contador = contador + paso;

					cab_s2 = cab_aux->sgte;

					final_aux = cab_aux;
					pivote = final_aux->sgte;
					sombrero = aux;



					contador1 = 0;

				}


			}
		}
	}
}

void Sort_shell(Tlista &cab)
{
	if (cab != NULL)
	{
		int n = 1;
		Tlista aux1 = cab;
		Tlista aux2 = aux1->sgte;
		bool bandera = false;

		while (aux2 != NULL)
		{
			aux2 = aux2->sgte;
			n++;
		}

		do
		{
			n = n / 2;
			int c = 1;

			aux1 = cab;
			aux2 = aux1->sgte;
			if (n != 0)
			{
				bandera = false;
				while (c  < n)
				{
					aux2 = aux2->sgte;
					c++;
				}

				while (aux2 != NULL)
				{
					if (aux1->valor > aux2->valor)
					{
						int aux = aux1->valor;
						aux1->valor = aux2->valor;
						aux2->valor = aux;
						bandera = true;
					}
					aux1 = aux1->sgte;
					aux2 = aux2->sgte;

				}

			}
			else
			{
				if (bandera == true)
				{
				while (aux2 != NULL)
				{
					if (aux1->valor > aux2->valor)
					{
						int aux = aux1->valor;
						aux1->valor = aux2->valor;
						aux2->valor = aux;
						bandera = true;
					}
					aux1 = aux1->sgte;
					aux2 = aux2->sgte;
				}

				}
				bandera = false;
			}

		} while (n != 0 || bandera == true);
	}
}


void burbuja(Tlista &lista){

	Tlista p = lista;
	bool flag = false;
	int aux;

	if(lista == NULL){
		cout<<"Lista vacia...\n\n";
	}else{

		if(lista -> sgte == NULL){
			mostrar(lista);
			cout<<"\n\n";
		}else{
			while(p->sgte != NULL){
				if(p->valor > p->sgte->valor){
					aux = p->valor;
					p->valor = p->sgte->valor;
					p->sgte->valor = aux;
					flag = true;
				}
				p=p->sgte;
				if(p->sgte ==NULL){
					if(flag){
						p=lista;
						flag = false;
					}else{
						break;
					}
				}
			}
			mostrar(lista);
			cout<<"\n\n";
		}
	}
}

void seleccion(Tlista &lista){

	int minimo =999999999;
	int pre =0;
	Tlista apuntadorauxiliar = lista;
	Tlista apuntadorauxiliar2 = lista;
	Tlista apuntadorauxiliar3 = lista;

	while (apuntadorauxiliar->sgte !=NULL){
		apuntadorauxiliar2 = apuntadorauxiliar->sgte;
		apuntadorauxiliar3 = apuntadorauxiliar;

		while (apuntadorauxiliar2!=NULL)
		{
			if (minimo > apuntadorauxiliar2->valor)
				minimo = apuntadorauxiliar2->valor;

		apuntadorauxiliar2 = apuntadorauxiliar2->sgte;
		}

		while (apuntadorauxiliar3 -> valor != minimo)
		{
			apuntadorauxiliar3 = apuntadorauxiliar3->sgte;
		}

		if (minimo < apuntadorauxiliar->valor){
			pre = apuntadorauxiliar -> valor;
			apuntadorauxiliar -> valor = minimo;
			apuntadorauxiliar3->valor = pre;
		}

		apuntadorauxiliar = apuntadorauxiliar->sgte;
		minimo = 50000000;
	}
}


void mostrar(Tlista &f)
{
    if(f==NULL)
    {
        cout<<"\n\tLista vacia"<<endl;
        return;
    }
    Tlista p=f;
    cout<<"\tLista : "<<endl<<endl;
    cout<<"\t";
    while(p->sgte!=NULL)
    {
        cout<<p->valor<<" - ";
        p=p->sgte;
    }

    cout<<p->valor<<endl;
}

int convertir(string num, bool &bandera)//La bandera me indica si fue correcto, con 1 -> fue un exito, con 0 -> fracaso
{
    int aux = 0;
    bandera = true;

    for(int i = 0 ; i < num.size() ; i++)
    {

        if((int) num.at(i) >= 48 && (int) num.at(i) <= 57 )
        {
            aux += ( (int)num.at(i) - 48) * pow(10,num.size() - (i+1) );
        }
        else
        {
            bandera = false;
            aux = -999;
            break;
        }

    }

    return aux;
}
